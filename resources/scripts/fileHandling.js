if(document.location.href.indexOf("mod.html") != -1){
    sendMod();
}

if(document.location.href.indexOf("listMods.html") != -1){
    requestListMods();
}

function searchFiles(dir){

    var xhttp = new XMLHttpRequest();
    xhttp.open('get','mods/'+dir,false);
    xhttp.send(null);
          
    var div = document.createElement("div");
    div.innerHTML = xhttp.responseText;
    return div.getElementsByTagName('a');

}


function sendMod(){
    var params = (new URL(document.location)).searchParams;
    var modNum = params.get("mod");
    var files = searchFiles(modNum);
    
    var file;
    files=Array.prototype.slice.call(files);
    for (var i = files.length - 1; i >= 0; i--) {
        element=files[i];
        if(element.innerText.search(".json")!=-1){
            file=element.innerText;
            break;
        }
    }

    if(window.XMLHttpRequest) var request = new XMLHttpRequest();
    else var request = new ActiveXObject('Microsoft.XMLHTTP');

    request.open('get','mods/' + modNum + '/' + file,false);
    request.send(null);

    parseMod(request.responseText);

    return;
}


function requestListMods(){
    var folders = searchFiles("");
    folders=Array.prototype.slice.call(folders);
    var message = [];
    folders.forEach(element => {
        var text = element.innerText;
        if(text.match(/[0-9]+\//gi) != null){
            var modName;
            files=searchFiles(text.replace('/',''));
            files=Array.prototype.slice.call(files);
            for (var i = files.length - 1; i >= 0; i--) {
                element=files[i];
                if(element.innerText.search(".json")!=-1){
                    modName=element.innerText.replace(".json","");
                    break;
                }
            }
            message.push(text.replace('/','')+":"+modName);
        }
    });

    listMods(message);
    return;
}

