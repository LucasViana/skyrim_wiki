function extractSpecial(str){
    var text = "";
    for(let i = 0; i < str.length; i++){
    var element = str.charAt(i);
        if(element != '}'){
            text += element;
        }
    }
    return text;
}

function parseSpecial(content, modName, media){
    var params = (new URL(document.location)).searchParams;
    var modNumber = params.get("mod");
    var onSpecial = false;
    var newContent = [];
    var src = "";
    var text = "";
    var attr = "";
    var pos = 0;
    newContent.push(document.createElement("p"));
    for(let i = 0; i < content.length; i++){
        var element = content.charAt(i);
        if(onSpecial){
            if(content.substr(i,4) == "pic:"){
                attr = "img";
                src = "mods/"+modNumber+"/";
                i += 3;
            }else if(content.substr(i,4) == "vid:"){
                attr = "video";
                i += 3;   
            }else if(content.substr(i,5) == "link:"){
                attr = "link";
                i += 4;
                
            }else{
                if(element == '}'){
                    onSpecial = false;
                    if(attr == "img"){
                        var img = document.createElement("img");
                        img.setAttribute("src", src+media.imgs[pos-1]);
                        img.setAttribute("alt", text);
                        newContent[newContent.length-1].appendChild(img);
                    }else if(attr == "video"){ 
                        var iframe = document.createElement("iframe");
                        iframe.setAttribute("src", media.videos[pos-1]);
                        iframe.setAttribute("width", "420");
                        iframe.setAttribute("height", "350");
                        newContent[newContent.length-1].appendChild(iframe);
                    }else if(attr == "link"){
                        var link = document.createElement("a");
                        link.setAttribute("href", media.links[pos-1]);
                        link.innerText = text;
                        newContent[newContent.length-1].appendChild(link);
                    }
                    newContent.push(document.createElement("p"));
                    attr = "";
                    src = "";
                    text = "";
                    pos = 0;
                }else{
                    if(element == '.'){
                        pos = parseInt(text);
                        text = "";
                    }else{
                        text += element;
                    }
                }
            }
            if(element == '}'){
                onSpecial = false;
                newContent.push(document.createElement("p"));
            }
        }else{
            if(element != '{'){
                if(element == "\n"){
                    newContent.push(document.createElement("p"));
                }else{
                    newContent[newContent.length-1].innerText += element;
                }
            }else{
                newContent.push(document.createElement("div"));
                newContent[newContent.length-1].setAttribute("id","media");
                onSpecial = true;
            }
        }
    }
    return newContent;
}



function parseMod(modFile){
    var mod = JSON.parse(modFile.replace(/&quot;/ig,'"'));
    var main = document.getElementById("main");
    var title = document.createElement("b");
    title.innerText = mod.name;
    main.appendChild(title);
    main.appendChild(document.createElement("br"));
    for (let i = 0; i < mod.headers.length; i++) {
        if(mod.headers[i] != "Unnamed"){
            var subtitle = document.createElement("b");
            subtitle.innerText = mod.headers[i];
            main.appendChild(subtitle);
        }
        var content = parseSpecial(mod.content[i], mod.name, mod.media);
        content.forEach(element => {
            main.appendChild(element);
        });
        main.appendChild(document.createElement("br"));
    }
}

function assemblyModList(mods){
    var list = [];
    var aux = "";
    var lastElement = '';
    mods.split("").forEach(element => {
        if(lastElement == '"' && (element == ',' || element == ']')){
            list.push(aux);
            aux = "";
        }else{
            if(!(element == '"' || element == '[' || element == "]")){
                aux += element;
                lastElement = "";
            }else{
                lastElement = element;
            }
        }

    });
    return list;
}

function listMods(mods){
    var list = document.getElementById("modList");
    mods.forEach(element => {
        element = element.split(':');
        var newEntry = document.createElement("li");
        var newLink = document.createElement("a");
        var path = "mod.html?mod=" + element[0];
        newLink.setAttribute("href", path);
        newLink.innerText = element[1];
        newEntry.appendChild(newLink);
        list.appendChild(newEntry)
    });
}
